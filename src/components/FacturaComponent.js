import TicketComponent from "./TicketComponent";
import PersonComponent from "./PersonComponent";
import {useState} from "react";

function FacturaComponent() {
    const [step, setStep] = useState(1);

    const nextStep = () => {
        setStep(step + 1);
    }

    const prevStep = () => {
        setStep(step - 1)
    }

    switch (step) {
        case 1:
            return <TicketComponent nextStep={nextStep}/>
        case 2:
            return <PersonComponent prevStep={prevStep}/>
    }

}

export default FacturaComponent;