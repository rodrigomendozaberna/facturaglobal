import Logo from '../assets/img/logop.png'

function TicketComponent(props) {
    return (
            <div className="relative md:gap-6">
                <dt>
                    <div className="mt-5 md:mt-0 md:col-span-2">
                        <form>
                            <div className="shadow-md sm:overflow-hidden">
                                <div className="px-4 py-5 bg-white space-y-6 sm:p-6">
                                    <div className="py-0">
                                        <div className="col-span-3 sm:col-span-2">
                                            <label htmlFor="company_website"
                                                   className="block text-sm font-medium text-gray-700">
                                                Restaurante / Empresa:</label>
                                            <div className="mt-1 flex rounded-md shadow-sm"><select
                                                className="focus:ring-indigo-500 focus:border-indigo-500 block w-full pl-7 pr-12 sm:text-sm border-gray-100 rounded-md bg-gray-100"
                                                name="codEmpresa" id="codEmpresa">
                                                <option value="30">Seleccionar</option>
                                                <option value="29">Tacos Pampas Zaragoza</option>
                                                <option value="7">Pampas Paseo de la República</option>
                                                <option value="4">Pampas Centro Sur</option>
                                                <option value="2">Pampas Plaza de Toros</option>
                                                <option value="1">Tacos Pampas Constituyentes</option>
                                            </select></div>
                                        </div>
                                        <div className="py-3"><label htmlFor="company_website"
                                                                     className="block text-sm font-medium text-gray-700">Número
                                            de folio del ticket:</label>
                                            <div className="mt-1 flex rounded-md shadow-sm"><input type="text"
                                                                                                   name="folio"
                                                                                                   required=""
                                                                                                   id="folio"
                                                                                                   className="focus:ring-indigo-500 focus:border-indigo-500 block w-full pl-7 pr-12 sm:text-sm border-gray-100 rounded-md bg-gray-100"
                                                                                                   placeholder="Ejm: 00000000"
                                                                                                   value=""/></div>
                                        </div>
                                        <div className="py-2"><label htmlFor="company_website"
                                                                     className="block text-sm font-medium text-gray-700">Fecha
                                            de consumo</label>
                                            <div className="mt-1 flex rounded-md shadow-sm"><input type="date"
                                                                                                   name="fechaConsumo"
                                                                                                   id="fechaConsumo"
                                                                                                   required=""
                                                                                                   className="focus:ring-indigo-500 focus:border-indigo-500 block w-full pl-7 pr-12 sm:text-sm border-gray-100 rounded-md bg-gray-100"
                                                                                                   placeholder="Ejm: 00000000"
                                                                                                   value=""/></div>
                                        </div>
                                        <div className="py-2"><label htmlFor="company_website"
                                                                     className="block text-sm font-medium text-gray-700">Total
                                            consumo:</label>
                                            <div className="mt-1 relative rounded-md shadow-sm">
                                                <div
                                                    className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                                                    <span className="text-gray-500 sm:text-sm">$</span></div>
                                                <input type="number" name="total" required="" id="total"
                                                       className="focus:ring-indigo-500 focus:border-indigo-500 block w-full pl-7 pr-12 sm:text-sm border-gray-100 rounded-md bg-gray-100"
                                                       placeholder="0.00" value=""/></div>
                                        </div>
                                        <div className="py-2"><label htmlFor="company_website"
                                                                     className="block text-sm font-medium text-gray-700">Cantidad
                                            a facturar:</label>
                                            <div className="mt-1 relative rounded-md shadow-sm">
                                                <div
                                                    className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                                                    <span className="text-gray-500 sm:text-sm">$</span></div>
                                                <input type="number" name="cantidadFactura" required=""
                                                       id="cantidadFactura"
                                                       className="focus:ring-indigo-500 focus:border-indigo-500 block w-full pl-7 pr-12 sm:text-sm border-gray-100 rounded-md bg-gray-100"
                                                       placeholder="0.00" value=""/></div>
                                        </div>
                                    </div>
                                </div>
                                <div className="px-4 py-3 bg-gray-50 text-right sm:px-6">
                                    <button type="submit"
                                            onClick={props.nextStep}
                                            className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-black hover:bg-yellow-300 hover:text-black focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">Siguiente
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </dt>
            </div>
    )
}

export default TicketComponent;