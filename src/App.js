import FacturaComponent from "./components/FacturaComponent";
import Logo from "./assets/img/logop.png";

function App() {
    return (
        <div className="container mx-auto">
            <div className="grid grid-cols-3 gap-4">
                <div>
                    <img
                        src="http://facturaciongrupopampas.com/Scripts/FGP3/static/media/GrupoPampas_Facturacion_Carnicerias.1ec45448.jpg"
                        alt=""/>
                </div>
                <div>
                    <div className="flex justify-center">
                        <img className="object-contain w-40 h-auto"
                             src={Logo} alt="logo"/>
                    </div>
                    <FacturaComponent/>
                </div>
                <div>
                    <img
                        src="http://facturaciongrupopampas.com/Scripts/FGP3/static/media/GrupoPampas_Facturacion_Pampas.294a1145.jpg"
                        alt=""/>
                </div>
            </div>
        </div>
    );
}

export default App;
