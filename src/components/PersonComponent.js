import Logo from "../assets/img/logop.png";

function PersonComponent(props) {
    return (
            <div className="relative md:gap-6">
                <dt>
                    <div className="mt-5 md:mt-0 md:col-span-2">
                        <form>
                            <div className="shadow-md sm:rounded-md sm:overflow-hidden">
                                <div className="px-4 py-5 bg-white space-y-6 sm:p-6">
                                    <div className="py-0">
                                        <div className="col-span-3 sm:col-span-2"><label htmlFor="company_website"
                                                                                         className="block text-sm font-medium text-gray-700">RFC
                                            del cliente:</label>
                                            <div className="mt-1 flex rounded-md shadow-sm"><input type="text"
                                                                                                   name="rfcClie"
                                                                                                   id="rfcClie"
                                                                                                   required=""
                                                                                                   className="focus:ring-indigo-500 focus:border-indigo-500 block w-full pl-7 pr-12 sm:text-sm border-gray-100 rounded-md bg-gray-100 uppercase"
                                                                                                   placeholder="Ejm: 00000000"
                                                                                                   value=""/></div>
                                        </div>
                                        <div className="py-3"><label htmlFor="company_website"
                                                                     className="block text-sm font-medium text-gray-700">Razón
                                            Social:</label>
                                            <div className="mt-1 flex rounded-md shadow-sm"><input type="text"
                                                                                                   name="razonSocialClie"
                                                                                                   required=""
                                                                                                   id="razonSocialClie"
                                                                                                   className="focus:ring-indigo-500 focus:border-indigo-500 block w-full pl-7 pr-12 sm:text-sm border-gray-100 rounded-md bg-gray-100"
                                                                                                   placeholder="Ejm: 00000000"
                                                                                                   value=""/></div>
                                        </div>
                                        <div className="py-2"><label htmlFor="company_website"
                                                                     className="block text-sm font-medium text-gray-700">Uso
                                            del CFDI:</label>
                                            <div className="mt-1 flex rounded-md shadow-sm"><select
                                                className="focus:ring-indigo-500 focus:border-indigo-500 block w-full pl-7 pr-12 sm:text-sm border-gray-100 rounded-md bg-gray-100"
                                                name="codUsoCFDI" id="codUsoCFDI">
                                                <option value="default" disabled="">Opciones..</option>
                                                <option value="1">Adquisición de mercancias</option>
                                                <option value="2">Devoluciones, descuentos o bonificaciones</option>
                                                <option value="3">Gastos en general</option>
                                                <option value="4">Construcciones</option>
                                                <option value="5">Mobilario y equipo de oficina por inversiones</option>
                                                <option value="6">Equipo de transporte</option>
                                                <option value="7">Equipo de computo y accesorios</option>
                                                <option value="8">Dados, troqueles, moldes, matrices y herramental
                                                </option>
                                                <option value="9">Comunicaciones telefónicas</option>
                                                <option value="10">Comunicaciones satelitales</option>
                                                <option value="11">Otra maquinaria y equipo</option>
                                                <option value="12">Honorarios médicos, dentales y gastos
                                                    hospitalarios.
                                                </option>
                                                <option value="13">Gastos médicos por incapacidad o discapacidad
                                                </option>
                                                <option value="14">Gastos funerales.</option>
                                                <option value="15">Donativos.</option>
                                                <option value="16">Intereses reales efectivamente pagados por créditos
                                                    hipotecarios (casa habitación).
                                                </option>
                                                <option value="17">Aportaciones voluntarias al SAR.</option>
                                                <option value="18">Primas por seguros de gastos médicos.</option>
                                                <option value="19">Gastos de transportación escolar obligatoria.
                                                </option>
                                                <option value="20">Depósitos en cuentas para el ahorro, primas que
                                                    tengan como base planes de pensiones.
                                                </option>
                                                <option value="21">Pagos por servicios educativos (colegiaturas)
                                                </option>
                                                <option value="22">Por definir</option>
                                            </select></div>
                                        </div>
                                        <div className="py-2"><label htmlFor="company_website"
                                                                     className="block text-sm font-medium text-gray-700">Correo
                                            del cliente:</label>
                                            <div className="mt-1 relative rounded-md shadow-sm"><input type="email"
                                                                                                       name="correo"
                                                                                                       required=""
                                                                                                       id="correo"
                                                                                                       className="focus:ring-indigo-500 focus:border-indigo-500 block w-full pl-7 pr-12 sm:text-sm border-gray-100 rounded-md bg-gray-100"
                                                                                                       placeholder="email@dominion.com"
                                                                                                       value=""/></div>
                                        </div>
                                    </div>
                                </div>
                                <div className="grid px-4 py-3 bg-gray-50  sm:px-6">
                                    <div className="col-start-1 col-end-3">
                                        <button type="reset"
                                                onClick={props.prevStep}
                                                className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-blackhover:bg-yellow-300 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">Regresar
                                        </button>
                                    </div>
                                    <div className="col-end-13">
                                        <button type="submit"
                                                className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-black hover:bg-yellow-300 hover:text-black focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">Realizar
                                            factura
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </dt>
            </div>
    )
}

export default PersonComponent;